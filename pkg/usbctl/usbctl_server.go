// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package usbctl

import (
	"fmt"
	"github.com/google/gousb"
	"github.com/google/gousb/usbid"
	pb "gitlab.com/redfield/usbctl/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net"
	"os/exec"
)

// Allow for policy based filtering of devices
func filterDevice(device *pb.UsbDevice) bool {
	// TODO: come up with filter format? some kinda json?

	// Filter out hub devices
	if device.Class == 9 {
		return false
	}

	return true
}

func usbDeviceList() []*pb.UsbDevice {
	var ret []*pb.UsbDevice

	ctx := gousb.NewContext()
	defer ctx.Close()

	// OpenDevices is used to find the devices to open.
	_, _ = ctx.OpenDevices(func(desc *gousb.DeviceDesc) bool {
		usbDev := pb.UsbDevice{
			Name:          usbid.Describe(desc),
			State:         0,
			AssignedUuid:  "",
			Bus:           uint32(desc.Bus),
			Port:          uint32(desc.Port),
			Addr:          uint32(desc.Address),
			Speed:         uint32(desc.Speed),
			Spec:          uint32(desc.Spec),
			DeviceVersion: uint32(desc.Device),
			VendorId:      uint32(desc.Vendor),
			ProductId:     uint32(desc.Product),
			Class:         uint32(desc.Class),
			Subclass:      uint32(desc.SubClass),
			Protocol:      uint32(desc.Protocol),
		}

		if filterDevice(&usbDev) {
			ret = append(ret, &usbDev)
		}

		return false
	})

	return ret
}

type server struct{}

func (s *server) Attach(ctx context.Context, in *pb.AttachRequest, opts ...grpc.CallOption) (*pb.AttachReply, error) {

	hostbus := fmt.Sprintf("hostbus=%v", in.GetDevice().GetBus())
	hostaddr := fmt.Sprintf("hostaddr=%v", in.GetDevice().GetAddr())
	var controller string

	if in.GetDevice().GetSpeed() == uint32(gousb.SpeedHigh) {
		controller = fmt.Sprintf("controller=%v", 1)
	} else if in.GetDevice().GetSpeed() == uint32(gousb.SpeedLow) || in.GetDevice().GetSpeed() == uint32(gousb.SpeedFull) {
		controller = fmt.Sprintf("controller=%v", 0)
	}

	attach, err := exec.Command("xl", "usbdev-attach", in.GetDomid(), hostbus, hostaddr, controller).Output()
	if err != nil {
		return &pb.AttachReply{Reply: "Failed to attach device!"}, err
	}

	return &pb.AttachReply{Reply: string(attach)}, nil
}

func (s *server) Detach(ctx context.Context, in *pb.DetachRequest, opts ...grpc.CallOption) (*pb.DetachReply, error) {
	out := fmt.Sprintf("xl usbdev-detach %v hostbus=%v hostaddr=%v", in.GetDomid(), in.GetDevice().GetBus(), in.GetDevice().GetAddr())
	log.Print(out)

	return &pb.DetachReply{Reply: out}, nil
}

func (s *server) DeviceList(ctx context.Context, in *pb.DeviceListRequest, opts ...grpc.CallOption) (*pb.DeviceListReply, error) {
	devices := usbDeviceList()

	deviceReply := pb.DeviceListReply{UsbDevices: devices}

	return &deviceReply, nil
}

// StartUsbCtlService is the entry point for starting usb-server.
func StartUsbCtlService(lis net.Listener) {
	grpcServer := grpc.NewServer()

	go startDBusAdapter(&server{})

	grpcServer.Serve(lis)
}
