// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package usbctl

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"syscall"

	"github.com/godbus/dbus"
	"github.com/golang/protobuf/jsonpb"
	pb "gitlab.com/redfield/usbctl/api"
	"golang.org/x/net/context"
)

const (
	grpcDBusDestination = "com.gitlab.redfield.api.usbctl"
	grpcDBusObjectPath  = "/com/gitlab/redfield/api/usbctl"
)

type usbDBusInterface struct {
	serv      *server
	marshaler jsonpb.Marshaler
}

func (u *usbDBusInterface) Attach(cmd string) (string, *dbus.Error) {
	var reply string
	var msg pb.AttachRequest

	// Unmarshal JSON string to protobuf message
	err := jsonpb.UnmarshalString(cmd, &msg)
	if err != nil {
		e := fmt.Errorf("could not unmarshal 'cmd': %v", err)
		return reply, dbus.MakeFailedError(e)
	}

	// Make call to usbctl-service to attach device
	r, err := u.serv.Attach(context.Background(), &msg)
	if err != nil {
		return reply, dbus.MakeFailedError(err)
	}

	// Marshal JSON string from reply
	log.Print(r)
	reply, err = u.marshaler.MarshalToString(r)
	if err != nil {
		e := fmt.Errorf("could not marshal JSON string: %v", err)
		return reply, dbus.MakeFailedError(e)
	}

	return reply, nil
}

func (u *usbDBusInterface) Detach(cmd string) (string, *dbus.Error) {
	var reply string
	var msg pb.DetachRequest

	// Unmarshal JSON string to protobuf message
	err := jsonpb.UnmarshalString(cmd, &msg)
	if err != nil {
		e := fmt.Errorf("could not unmarshal 'cmd': %v", err)
		return reply, dbus.MakeFailedError(e)
	}

	// Make call to usbctl-service to attach device
	r, err := u.serv.Detach(context.Background(), &msg)
	if err != nil {
		return reply, dbus.MakeFailedError(err)
	}

	// Marshal JSON string from reply
	reply, err = u.marshaler.MarshalToString(r)
	if err != nil {
		e := fmt.Errorf("could not marshal JSON string: %v", err)
		return reply, dbus.MakeFailedError(e)
	}

	return reply, nil
}

func (u *usbDBusInterface) DeviceList() (string, *dbus.Error) {
	var reply string
	msg := pb.DeviceListRequest{}

	// Make call to usbctl-service to attach device
	r, err := u.serv.DeviceList(context.Background(), &msg)
	if err != nil {
		return reply, dbus.MakeFailedError(err)
	}

	// Marshal JSON string from reply
	reply, err = u.marshaler.MarshalToString(r)
	fmt.Printf(reply)

	if err != nil {
		e := fmt.Errorf("could not marshal JSON string: %v", err)
		return reply, dbus.MakeFailedError(e)
	}

	return reply, nil
}

func (u *usbDBusInterface) VmList() (string, *dbus.Error) {
	reply, err := exec.Command("xl", "list", "-l").Output()

	if err != nil {
		return string(reply), dbus.MakeFailedError(err)
	}

	return string(reply), nil
}

func startDBusAdapter(s *server) {
	// Create JSON marshaler
	m := jsonpb.Marshaler{
		EnumsAsInts:  true,
		EmitDefaults: true,
		Indent:       "\t",
		OrigName:     true,
		AnyResolver:  nil,
	}

	// Create USB DBus Interface
	u := usbDBusInterface{
		serv:      s,
		marshaler: m,
	}

	bus, err := dbus.SystemBus()
	if err != nil {
		log.Print(err)
		return
	}

	rn, err := bus.RequestName(grpcDBusDestination, dbus.NameFlagDoNotQueue)
	if err != nil {
		log.Print(err)
		return
	}
	defer bus.ReleaseName(grpcDBusDestination)

	// Make sure we own the name
	if rn != dbus.RequestNameReplyPrimaryOwner {
		log.Print("name '%v' is already in use on system bus")
		return
	}

	// Export DBus interface
	err = bus.Export(&u, grpcDBusObjectPath, grpcDBusDestination)
	if err != nil {
		log.Print(err)
		return
	}

	// Run until interrupted
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	<-c

	os.Exit(1)
}
